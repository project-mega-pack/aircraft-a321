# Aircraft Mega Pack A321

To install, simply drop `AMP_A321` into your Community Folder.

To create liveries, use the `AMP_A321_Livery_Template`.

Microsoft Flight Simulator © Microsoft Corporation. The Aircraft Mega Pack A321 was created under Microsoft's "Game Content Usage Rules" using assets from Microsoft Flight Simulator X, and it is not endorsed by or affiliated with Microsoft.

# Contributors

- Clink123#4567 - Founder of Project Mega Pack
- wassabi_sniper🇨🇦#1087 - Founder of Canadian Mods
- [Z+3] -Pro-pilot#5218 - Developer
- Berinchtein#3663 - Textures